<!--
http://overpass-api.de/api/status
https://www.retsinformation.dk/Forms/R0710.aspx?id=200564
https://www.retsinformation.dk/eli/lta/2017/136
http://danmarksadresser.dk/adressedata
-->
<!DOCTYPE html>
<head>
	<meta charset="uft-8"/>
	<title>autoAWS</title>
	<link rel="stylesheet" type="text/css" href="autoaws.css">
	<script type="text/javascript">
	function play_sound(sound) {
		var audioElement = document.createElement('audio');
		audioElement.setAttribute('src', sound+'.wav');
		audioElement.setAttribute('autoplay', 'autoplay');
		audioElement.load();
		audioElement.play();
	}
	</script>
</head>

<?php
error_reporting(0);
ini_set('default_charset', 'utf-8');
ini_set('memory_limit', '2048M'); //standard memory size not sufficient when downloading AWS data
set_time_limit(5400); //90 minutes
define('THIS', 'autoAWS 1.1'); //name and version of this script
define('URL',			'https://api.openstreetmap.org/api/0.6/');	//url to the OSM API
define('USERNAME',		'autoAWS');		//OSM username
define('PASSWORD',		'secret');	//OSM password
define('DEBUG',			FALSE);			//disables all cURL calls to the OSM API
define('OUTPUT',		FALSE);			//whether or not to output script progress to the browser (disabled by default for server version)
define('CONTINUOUS',	FALSE);			//reload the html page 1 second after an update is finished, thus immediately starting a new one (disabled by default for server version)
define('MANUAL',		FALSE);			//manually select which postcode to update (should only be used with CONTINUOUS=FALSE)
define('MANUAL_CODE',	4720);			//the postcode to update if MANUAL is TRUE
define('UPDATE_ALL',	FALSE);			//update all address nodes, even if nothing has changed. Useful when changing the tagging scheme
define('UPDATE_BY_TAG', FALSE);			//force an update of an address node if a specific tag (defined as UPDATE_TAG) is present
define('UPDATE_TAG', 'osak:revision');	//the tag to detect if UPDATE_BY_TAG is set to true


if(isset($_GET['postnr'])) {
	manual($_GET['postnr']);
}
elseif(isset($_GET['cleardb'])) {
	database_cleanup();
	output(6);
}
elseif(isset($_GET['passcode'])) {
	if($_GET['passcode'] == 'secret') {
		main();
	}
}


##### Main #####

function main() {
	ob_start();
	select_postcode();
	output(0, 'Adresseropdatering for postnummer ' . POSTNUMMER . ' (' . postnummer_navn(POSTNUMMER) . ')');
	if(DEBUG) {output(9, 'DEBUG MODE');}
	output(2, 'Undlad at lukke denne side indtil opdateringen er færdig.');
	output(1, 'Indlæser eksisterende adresser fra OSM');
	$osm = (array) download_osm_data();
	output(2, $osm[0] . ' adresser hentet fra OSM');
	if(!empty($osm[1])) {
		output(9, 'Der var problemer med følgende adresser i OSM. De bør tjekkes manuelt:');
		foreach($osm[1] AS $osm_fejl) {
			output(2, $osm_fejl);
		}
	}
	output(1, 'Indlæser rettelser til vejnavne');
	output(2, download_ois_fixes() . ' rettede vejnavne indlæst');
	output(1, 'Indlæser adresser fra Danmarks Adresseregister');
	$aws = (array) download_aws_data();
	output(2, $aws[0] . ' adresser hentet fra DAR');
	if(!empty($aws[1])) {
		output(9, 'Der var problemer med følgende adresser i DAR. De bør tjekkes manuelt:');
		foreach($aws[1] AS $aws_fejl) {
			output(2, $aws_fejl);
		}
	}
	output(1, 'Sammenligner adresser');
	detect_updates();
	detect_add();
	detect_delete();
	$count = (array) count_updates();
	if($count[0] == 0) {
		output(2, 'Der er ingen rettelser siden sidste opdatering');
		define('CHANCE', 100); //database_cleanup needs CHANCE to be defined, we won't actually use it for anything here
		database_cleanup();
		output(7);
		if(CONTINUOUS) {
			echo '<script>setTimeout(location.reload.bind(location), 1000);</script>';
		}
		die();
	}
	output(2, $count[1] . ' adresser skal opdateres');
	output(2, $count[2] . ' adresser skal tilføjes');
	output(2, $count[3] . ' adresser skal slettes');
	split_changeset($count[0]);
	if(CHANCE < 100) {
		output(2, 'Mange rettelser. Opdateringen vil blive delt i flere changesets. Der kan gå flere timer før hele postnummeret er opdateret.');
	}
	output(1, 'Opdaterer adresser i OSM');
	create_changeset();
	update_nodes();
	if(CHANCE == 100) { //delete and create MUST be run in the same script execution, otherwise we might lose extra nodes
		output(1, 'Sletter gamle adresser fra OSM');
		delete_nodes();
		output(1, 'Uploaded nye adresser til OSM');
		create_nodes();
	}
	close_changeset();
	database_cleanup();
	output(1, 'Opdatering færdig');
	output(2, '<a href="https://www.openstreetmap.org/changeset/'.CHANGESET.'">Changeset '.CHANGESET.'</a>');
	if(CONTINUOUS) {
		echo '<script>setTimeout(location.reload.bind(location), 1000);</script>';
	}
}

function manual(string $input) {
	//if the script is called with the GET value postnr set, an update of that postcode will be prioritized next time the script is run
	//this will only work if the given postcode has not already been updated within the last 5 days
	//the updated is not triggered directly, but the last_update date of the postcode is set to a point in the past that will force the script to select that postcode next time an update is run
	if(preg_match('/[0-9]{4}/', $input) == 1) { //input validation
		$nr = (int) $input;
		opdater_postnumre();
		$db = db_connect();
		$stmt = $db->prepare('SELECT `opdateret` FROM `last_update` WHERE `last_update`.`postnummer` = ?');
		$stmt->execute([$nr]);
		$result = $stmt->fetch();
		if(!$result) { //postcode does not exist
			output(9, 'Postnummer ' . $nr . ' findes ikke!');
			return;
		}
		$updated = new DateTime($result['opdateret']);
		$updated->add(new DateInterval('P5D'));
		$today = new DateTime();
		if($updated>$today) {
			output(9, 'Postnummer ' . $nr . ' er sidst opdateret for mindre end 5 dage siden, og vil derfor ikke blive opdateret igen. Prøv igen senere.');
			return;
		}
		$db->exec('UPDATE `last_update` SET `last_update`.`opdateret` = "2017-01-01 00:00:02" WHERE `last_update`.`postnummer` = ' . $nr);
		output(1, 'Postnummer ' . $nr . ' (' . postnummer_navn($nr) . ') vil snart blive opdateret');
		output(2, 'Bemærk venligst, at der kan gå nogle timer før opdateringen finder sted');
	}
	else {
		output(9, 'Ugyldigt postnummer');
	}
}

## Small helper functions ##
function output(int $level, string $text = '') {
	//outputs text and/or sound to the browser. Used to keep user aware of the progress of the script
	//string needs to be at least 4096 characters, otherwise flush() will not work across all browsers
	if(!OUTPUT) {
		return;
	}
	echo str_pad('',4096);
	switch($level) {
		//heading 1 text
		case 0:
			echo '<h1>' . $text . '</h1>';
		break;
		//heading 2 text
		case 1:
			echo '<h2>' . $text . '</h2>';
		break;
		//default text
		case 2:
			echo '<p>' . $text . '</p>';
		break;
		//sound to play when database is manually emptied
		case 6:
			echo '<script type="text/javascript">play_sound("clr");</script>';
		break;
		//sound to play when there are no changes
		case 7:
			echo '<script type="text/javascript">play_sound("non");</script>';
		break;
		//sound to play when update is finished
		case 8:
			echo '<script type="text/javascript">play_sound("fin");</script>';
		break;
		//error sound and text
		case 9:
			echo '<script type="text/javascript">play_sound("err");</script>';
			echo '<p class="error">' . $text . '</p>';
		break;
	}
	//sleep is also somehow needed to get flush() to work properly
	sleep(0);
	flush();
	ob_flush();
}

function postnummer_navn(int $nr): string {
	//returns the name of the given postcode
	$string = file_get_contents('https://dawa.aws.dk/postnumre/' . $nr);
	if(!$string) {
		output(9, 'Postnummeret ' . $nr . ' findes ikke!');
		return ''; //do not exit the script here since we still want to run the update in case the postcode was recently deleted
	}
	$json = (array) json_decode($string, true);
	return $json['navn'];
}

function db_connect(): PDO {
	//connects to database using PDO and returns a PDO object
	$host = 'localhost';
	$db   = 'autoaws';
	$user = '';
	$pass = '';
	$charset = 'utf8mb4';

	$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
	$opt = [
		PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
		PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
		PDO::ATTR_EMULATE_PREPARES   => false,
	];
	return new PDO($dsn, $user, $pass, $opt);
}

function add_tag(SimpleXMLElement $node, string $tags) {
	//adds a new <tag> element to the first child of the given XML object
	$tags = explode('=', $tags);
	$tag = $node->children()[0]->addChild('tag');
	$tag->addAttribute('k', $tags[0]);
	$tag->addAttribute('v', $tags[1]);
}

function street_name(array $fixes, string $navn, string $str_no, string $mun_no): string {
	//searches the array of ois fixes for the given mun&street code
	//returns the fixed name if there is one, otherwise returns the original name
	if(empty($fixes)) {
		return $navn;
	}
	foreach($fixes as $fix) {
		if($fix['mun_no'] == $mun_no AND $fix['str_no'] == $str_no) {
			return $fix['fixed_name'];
		}
	}
	return $navn;
}

function street_name_fixes(): array {
	//loads OIS fixes from the database into an array
	$fixes = array();
	$db = db_connect();
	$stmt = $db->prepare('SELECT `fixed_name`, `mun_no`, `str_no` FROM `ois_fixes` WHERE 1');
	$stmt->execute();
	$results = $stmt->fetchAll();
	$db = NULL;
	if(!$results) { //no fix available/needed
		return $fixes; //empty array
	}
	foreach($results as $result) {
		array_push($fixes, array('mun_no' => $result['mun_no'], 'str_no' => $result['str_no'], 'fixed_name' => $result['fixed_name']));
	}
	return $fixes;
}

function id_format(string $raw): string {
	//takes a possibly ill-formatted address ID and returns it in correct format
	$string = strtolower(str_replace('-','',$raw));
	if(strlen($string) != 32) {
		return $raw; //input looks weird, gives up
	}
	$string = substr_replace($string, '-', 8, 0);
	$string = substr_replace($string, '-', 13, 0);
	$string = substr_replace($string, '-', 18, 0);
	$string = substr_replace($string, '-', 23, 0);
	if(preg_match('/^([0-9a-f]{8}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{12})$/', $string)) {
		return $string;
	}
	return $raw;
}

## Functions used to download data and work on that data in the database ##
function download_osm_data(): array {
	//via overpass API, downloads all nodes which has the osak:identifier key and the selected postcode value
	//inserts the returned JSON data into database table 'osmdata'
	//downloads the corresponding xml representation of nodes from OSM
	$string = file_get_contents('http://overpass-api.de/api/interpreter?data=[out:json];node[%22osak:identifier%22][%22addr:postcode%22=' . POSTNUMMER . '];out;');
	if(!$string) {
		output(9, 'Kunne ikke kontakte overpass API');
		if(CONTINUOUS) {
			echo '<script>setTimeout(location.reload.bind(location), 1000);</script>';
		}
		die();
	}
	$json = (array) json_decode($string, true);
	$db = db_connect();
	//if the table is not empty, we assume the database is in use by another process
	if($db->query('SELECT * FROM `osmdata`')->fetch()) {
		output(9, 'Opdatering kører allerede');
		die();
	}
	if(empty($string)) { //nothing was downloaded, no addresses with the given postcode exist in OSM
		return array(0, array());
	}
	$stmt = $db->prepare('INSERT INTO `osmdata` (`osak:identifier`, `node_id`, `lat`, `lon`, `addr:city`, `addr:country`, `addr:housenumber`, `addr:postcode`, `addr:street`, `addr:municipality`, `addr:place`, `autoaws_ignore`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
	$error = array();
	$node_ids = array();
	foreach($json['elements'] AS $addr){
		$a = (string) strtoupper(str_replace('-','',$addr['tags']['osak:identifier'])); //strip osak:id for dashes and convert to uppercase. Previous import script used to do this for unknown reasons. Retained for easier backward compatability.
		$b = (int) $addr['id'];
		$c = (float) $addr['lat'];
		$d = (float) $addr['lon'];
		$e = isset($addr['tags']['addr:city']) ? $addr['tags']['addr:city'] : NULL;
		if(UPDATE_BY_TAG) {
			$e = isset($addr['tags'][UPDATE_TAG]) ? 'invalid' : $e; //by setting city name to "invalid" in the database, the DAR<=>OSM data check will fail for this address, thus triggering an update
		}
		$f = isset($addr['tags']['addr:country']) ? $addr['tags']['addr:country'] : NULL;
		$g = isset($addr['tags']['addr:housenumber']) ? $addr['tags']['addr:housenumber'] : NULL;
		$h = isset($addr['tags']['addr:postcode']) ? $addr['tags']['addr:postcode'] : NULL;
		$i = isset($addr['tags']['addr:street']) ? $addr['tags']['addr:street'] : NULL;
		$j = isset($addr['tags']['addr:municipality']) ? $addr['tags']['addr:municipality'] : NULL;
		$k = isset($addr['tags']['addr:place']) ? $addr['tags']['addr:place'] : NULL;
		$l = 0;
		if(isset($addr['tags']['autoaws']) AND strtolower($addr['tags']['autoaws']) == 'ignore') {
			$l = 1;
		}
		try {
			$stmt->execute([$a,$b,$c,$d,$e,$f,$g,$h,$i,$j,$k,$l]);
		}
		//in case of an adress resulting in a database error, skip it and move on to the next one
		//some addresses might get skipped, but creating error handling for every scenario is almost impossible,
		//since OSM data is not validated in any way, and can contain any number of errors
		catch(PDOException $ex) {
			switch($ex->getCode()) {
				case 22001: //string too long, probably wrong osak:identifier (must be 32 characters)
					delete_osm_node($b, $c, $d);
					break;
				case 23000: //duplicate osak:identifier on multiple nodes
					delete_osm_node($b, $c, $d);
					break;
				default:
					array_push($error, $i.' '.$g.', '.$h.' '.$e);
					break;
			}
		}
		array_push($node_ids, $b);
		if(count($node_ids) > 99) { //download full XML nodes from OSM in chunks of 100
			$impl = (string) implode(',', $node_ids);
			$url = URL . 'nodes?nodes=' . $impl;
			try {
				$nodes = new SimpleXMLElement(file_get_contents($url));
			}
			catch(Exception $ex) {
				output(9, 'Fejl i download af data fra OSM API');
				database_cleanup();
				die();
			}
			foreach($nodes->node AS $child) {
				$stmt1 = $db->prepare('INSERT INTO `xmlnodes`(`node_id`, `xml`) VALUES (?,?)');
				$id = (int) $child->attributes()->id[0];
				$xml = (string) $child->asXML();
				$stmt1->execute([$id,$xml]);
			}
			$node_ids = array();
		}
	}
	if(!empty($node_ids)) { //download the last few nodes (in case the last chunk is not exactly 100 nodes long)	
		$impl = (string) implode(',', $node_ids);
		$url = URL . 'nodes?nodes=' . $impl;
		try {
			$nodes = new SimpleXMLElement(file_get_contents($url));
			}
		catch(Exception $ex) {
			output(9, 'Fejl i download af data fra OSM API');
			database_cleanup();
			die();
		}
		foreach($nodes->node AS $child) {
			$stmt1 = $db->prepare('INSERT INTO `xmlnodes`(`node_id`, `xml`) VALUES (?,?)');
			$id = (int) $child->attributes()->id[0];
			$xml = (string) $child->asXML();
			$stmt1->execute([$id,$xml]);
		}
		$node_ids = array();
	}
	$antal = $db->query('select count(*) from `osmdata`')->fetchColumn();
	$db = NULL;
	return array($antal, $error);
}

function download_aws_data(): array {
	//via AWS API, downloads address data for the given postcode
	//inserts the returned JSON data into database table 'awsdata'
	//closes the database connection and returns the number of rows in 'awsdata'
	$string = file_get_contents('http://dawa.aws.dk/adgangsadresser?postnr=' . POSTNUMMER);
	if($string === FALSE) {
		output(9, 'Kunne ikke kontakte AWS API');
		die();
	}
	$json = json_decode($string, true);
	$street_name_fixes = street_name_fixes();
	$db = db_connect();
	if($db->query('SELECT * FROM `awsdata`')->fetch()) {
		die('Opdatering kører allerede');
	}
	$stmt = $db->prepare('INSERT INTO `awsdata` (`osak:identifier`, `lat`, `lon`, `postnummer_navn`, `husnr`, `postnummer_nr`, `vejstykke_navn`, `kommune_navn`, `sup_bynavn`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)');
	$error = array();
	foreach($json as $addr){
		$a = (string) strtoupper(str_replace('-','',$addr['id']));
		$b = (float) number_format($addr['adgangspunkt']['koordinater'][1], 6, '.', ''); //the old script used to update addresses only worked with 6 decimals of precision in coordinates, for unknown reasons. This limit is retained since otherwise it becomes difficult to compare address locations
		$c = (float) number_format($addr['adgangspunkt']['koordinater'][0], 6, '.', '');
		//$b = (float) $addr['adgangspunkt']['koordinater'][1];
		//$c = (float) $addr['adgangspunkt']['koordinater'][0];
		$d = (string) $addr['postnummer']['navn'];
		$e = (string) $addr['husnr'];
		$f = (int) $addr['postnummer']['nr'];
		$g = (string) street_name($street_name_fixes, $addr['vejstykke']['navn'], $addr['vejstykke']['kode'], $addr['kommune']['kode']);
		$h = (string) $addr['kommune']['navn'];
		$i = $addr['supplerendebynavn'];
		if($i == $d) { //in some places in DAR, the supplerende bynavn is set even though it is equal to the postnummer navn. Let's remove that.
			$i = NULL;
		}
		//discard address if it is outside 54N007E-58N016E (roughly Denmark)
		if($b < 54 OR $b > 58 OR $c < 7 OR $c > 16) {
			array_push($error, $g.' '.$e.', '.$f.' '.$d.' ('.$h.')');
			continue;
		}
		try {
			$stmt->execute([$a,$b,$c,$d,$e,$f,$g,$h,$i]);
		}
		//we generally do not expect any errors here since AWS data should mostly be of good quality.
		catch(PDOException $ex) {
			array_push($error, $g.' '.$e.', '.$f.' '.$d);
			continue;
		}
	}
	$antal = $db->query('select count(*) from `awsdata`')->fetchColumn();
	$db = NULL;
	return array($antal, $error);
}

function download_ois_fixes(): int {
	//downloads a list of street name fixes from https://oisfixes.iola.dk and loads them into the ois_fixes database table
	$db = db_connect();
	$stmt = $db->prepare('INSERT INTO `ois_fixes`(`fixed_name`, `mun_no`, `str_no`) VALUES (?, ?, ?)');
	$results = json_decode(file_get_contents('https://oisfixes.iola.dk/api/getwaycorrections/'));
	$datas = $results->data;
	foreach($datas as $data) {
		$fixed_name = $data[1];
		$mun_no = $data[2];
		$str_no = $data[3];
		try {
			$stmt->execute([$fixed_name, $mun_no, $str_no]);
		}
		catch(PDOException $ex) {
			continue;
		}
	}
	$antal = $db->query('select count(*) from `ois_fixes`')->fetchColumn();
	$db = NULL;
	return $antal;
}

function create_changeset() {
	//calls the OSM API to open a new changeset
	//returns the new changeset ID
	$changeset = new SimpleXMLElement('<osm><changeset></changeset></osm>');
	add_tag($changeset, "created_by=".THIS);
	add_tag($changeset, "comment=Adresser opdateret for postnummer " . POSTNUMMER);
    add_tag($changeset, "source=Danmarks Adresseregister");
	add_tag($changeset, "bot=yes");
	$ch = curl_init();
	curl_setopt_array($ch, array(
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
		CURLOPT_USERPWD => USERNAME.":".PASSWORD,
		CURLOPT_CUSTOMREQUEST =>"PUT",
		CURLOPT_POSTFIELDS => html_entity_decode($changeset->asXML()),
		CURLOPT_URL => URL.'changeset/create')
	);
	if(!DEBUG) {
		$changeset_id = (int) curl_exec($ch);
		$resp = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		switch($resp) {
			case 400:
				output(9, 'Kritisk fejl. OSM API PUT changeset/create returnerede 400. Opdatering afbrudt.');
			die();
			case 405:
				output(9, 'Kritisk fejl. OSM API PUT changeset/create returnerede 405. Opdatering afbrudt.');
			die();
			default:
				define('CHANGESET', $changeset_id);
			break;
		}
	}
	curl_close($ch);
	if(DEBUG) { //the CHANGESET const should still be defined if we are in debug mode, since many functions rely on it
		define('CHANGESET', 1234);
		return;
	}
	return;
}

function close_changeset(int $chg = CHANGESET) {
	$ch = curl_init();
	curl_setopt_array($ch, array(
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
		CURLOPT_USERPWD => USERNAME.":".PASSWORD,
		CURLOPT_CUSTOMREQUEST =>"PUT",
		CURLOPT_URL => URL.'changeset/'.$chg.'/close')
	);
	if(!DEBUG) {
		curl_exec($ch);
	}
	curl_close($ch);
	output(8);
}

function detect_add() {
	//inserts addresses in `add_addr` that exist in the `awsdata` table but not in the `osmdata` table (compared on the osak:identifier value)
	$db = db_connect();
	$sql = '
		INSERT INTO     `add_addr`(`osak:identifier`, `lat`, `lon`, `addr:city`, `addr:country`, `addr:housenumber`, `addr:postcode`, `addr:street`, `addr:municipality`, `addr:place`)
		SELECT          `awsdata`.`osak:identifier`, `awsdata`.`lat`, `awsdata`.`lon`, `awsdata`.`postnummer_navn`, "DK", `awsdata`.`husnr`, `awsdata`.`postnummer_nr`, `awsdata`.`vejstykke_navn`, `awsdata`.`kommune_navn`, `awsdata`.`sup_bynavn`
		FROM            `awsdata`
		WHERE           `awsdata`.`osak:identifier` NOT IN(
			SELECT      `osmdata`.`osak:identifier`
			FROM        `osmdata`
			WHERE       1);
		';
	$db->exec($sql);
	$db = NULL;
}

function detect_updates() {
	//compares addresses in `awsdata` and `osmdata` and adds addresses to `update_addr` if an update is needed
	$db = db_connect();
	$sql = '
		INSERT INTO    `update_addr`(`osak:identifier`, `node_id`, `lat`, `lon`, `addr:city`, `addr:country`, `addr:housenumber`, `addr:postcode`, `addr:street`, `addr:municipality`, `addr:place`)
		SELECT         `awsdata`.`osak:identifier`, `osmdata`.`node_id`, `awsdata`.`lat`, `awsdata`.`lon`, `awsdata`.`postnummer_navn`, "DK", `awsdata`.`husnr`, `awsdata`.`postnummer_nr`, `awsdata`.`vejstykke_navn`, `awsdata`.`kommune_navn`, `awsdata`.`sup_bynavn`
		FROM           `awsdata`
		INNER JOIN     `osmdata` ON `awsdata`.`osak:identifier` = `osmdata`.`osak:identifier`
		WHERE         (`osmdata`.`lat` <> `awsdata`.`lat`
			OR         `osmdata`.`lon` <> `awsdata`.`lon`
			OR         `osmdata`.`addr:city` <> `awsdata`.`postnummer_navn`
			OR		   `osmdata`.`addr:city` IS NULL
			OR         `osmdata`.`addr:housenumber` <> `awsdata`.`husnr`
			OR         `osmdata`.`addr:housenumber` IS NULL
			OR         `osmdata`.`addr:street` <> `awsdata`.`vejstykke_navn`
			OR         `osmdata`.`addr:street` IS NULL
			OR         `osmdata`.`addr:municipality` <> `awsdata`.`kommune_navn`
			OR		   `osmdata`.`addr:municipality` IS NULL
			OR		   `osmdata`.`addr:place` <> `awsdata`.`sup_bynavn`
			OR		   (`osmdata`.`addr:place` IS NOT NULL AND `awsdata`.`sup_bynavn` IS NULL)
			OR		   (`osmdata`.`addr:place` IS NULL AND `awsdata`.`sup_bynavn` IS NOT NULL)
					   )
		AND            `osmdata`.`autoaws_ignore` = 0;
		';
	//forces an update of all addresses, useful if the tagging structure has been changed
	if(UPDATE_ALL) {
		$sql = '
		INSERT INTO    `update_addr`(`osak:identifier`, `node_id`, `lat`, `lon`, `addr:city`, `addr:country`, `addr:housenumber`, `addr:postcode`, `addr:street`, `addr:municipality`, `addr:place`)
		SELECT         `awsdata`.`osak:identifier`, `osmdata`.`node_id`, `awsdata`.`lat`, `awsdata`.`lon`, `awsdata`.`postnummer_navn`, "DK", `awsdata`.`husnr`, `awsdata`.`postnummer_nr`, `awsdata`.`vejstykke_navn`, `awsdata`.`kommune_navn`, `awsdata`.`sup_bynavn`
		FROM           `awsdata`
		INNER JOIN     `osmdata` ON `awsdata`.`osak:identifier` = `osmdata`.`osak:identifier`
		WHERE          `osmdata`.`autoaws_ignore` = 0;
		';
	}
	$db->exec($sql);
	$db = NULL;
}

function detect_delete() {
	//inserts addresses in `delete_addr` that exist in the `osmdata` table but not in the `awsdata` table (compared on the osak:identifier value)
	$db = db_connect();
	$sql = '
		INSERT INTO     `delete_addr`(`node_id`, `lat`, `lon`)
		SELECT          `osmdata`.`node_id`, `osmdata`.`lat`, `osmdata`.`lon`
		FROM            `osmdata`
		WHERE           `osmdata`.`osak:identifier` NOT IN(
			SELECT      `awsdata`.`osak:identifier`
			FROM        `awsdata`
			WHERE       1)
		AND            `osmdata`.`autoaws_ignore` = 0;
		';
	try {
		$db->exec($sql);
	}
	catch(PDOException $ex) {
		//do nothing
	}
	$db = NULL;
	detect_delete_add();
}

function detect_delete_add() {
	//finds cases where an address node is going to be deleted and a new node created again at the same position
	//removes such addresses from the `add_addr` and `delete_addr` tables and inserts them into `update_addr` instead
	//this means that the existing node will be updated instead of being deleted and re-added
	$db = db_connect();
	try {
		$db->exec('
		INSERT INTO			`update_addr`(`osak:identifier`, `node_id`, `lat`, `lon`, `addr:city`, `addr:country`, `addr:housenumber`, `addr:postcode`, `addr:street`, `addr:municipality`, `addr:place`)
		SELECT				`add_addr`.`osak:identifier`, `delete_addr`.`node_id`, `add_addr`.`lat`, `add_addr`.`lon`, `add_addr`.`addr:city`, `add_addr`.`addr:country`, `add_addr`.`addr:housenumber`, `add_addr`.`addr:postcode`, `add_addr`.`addr:street`, `add_addr`.`addr:municipality`, `add_addr`.`addr:place`
		FROM				`add_addr`
		INNER JOIN			`delete_addr` ON `add_addr`.`lat` = `delete_addr`.`lat` AND `add_addr`.`lon` = `delete_addr`.`lon`
		');
	}
	catch (Exception $ex) {
		//do nothing
	}

	//if an address now exists in update_addr, remove it from add_addr and/or delete_addr
	$db->exec('
	DELETE FROM `add_addr`
	WHERE `add_addr`.`osak:identifier` IN(
		SELECT `update_addr`.`osak:identifier`
		FROM `update_addr`
		WHERE 1);
	');
	$db->exec('
	DELETE FROM `delete_addr`
	WHERE `delete_addr`.`node_id` IN(
		SELECT `update_addr`.`node_id`
		FROM `update_addr`
		WHERE 1);
	');

	//select OSM nodes with duplicate osak:identifier tags
	$sql = '
		INSERT INTO     `delete_addr`(`node_id`, `lat`, `lon`)
		SELECT          `invalid_node`.`node_id`, `invalid_node`.`lat`, `invalid_node`.`lon`
		FROM            `invalid_node`
		WHERE           `invalid_node`.`node_id` NOT IN(
			SELECT      `osmdata`.`node_id`
			FROM        `osmdata`
			WHERE       1);
		';
	try {
		$db->exec($sql);
	}
	catch(PDOException $ex) {
		//do nothing
	}
	//import the xml node string into the update and delete tables
	$db->exec('
	UPDATE `update_addr`
	JOIN `xmlnodes` ON `update_addr`.`node_id` = `xmlnodes`.`node_id`
	SET `update_addr`.`xml` = `xmlnodes`.`xml`
	WHERE `update_addr`.`node_id` = `xmlnodes`.`node_id`
	');
	$db->exec('
	UPDATE `delete_addr`
	JOIN `xmlnodes` ON `delete_addr`.`node_id` = `xmlnodes`.`node_id`
	SET `delete_addr`.`xml` = `xmlnodes`.`xml`
	WHERE `delete_addr`.`node_id` = `xmlnodes`.`node_id`
	');
	$db = NULL;
}

function count_updates(): array {
	//returns the number of records in the update, add and delete tables as well as their sum
	$db = db_connect();
	$update = (int) $db->query('select count(*) from `update_addr`')->fetchColumn();
	$add = (int) $db->query('select count(*) from `add_addr`')->fetchColumn();
	$delete = (int) $db->query('select count(*) from `delete_addr`')->fetchColumn();
	$sum = (int) $update+$add+$delete;
	$db = NULL;
	return array($sum, $update, $add, $delete);
}

function delete_osm_node(int $node, float $lat, float $lon) {
	//inserts a given node ID into the invalid_node table
	//this node will be added to delete_addr later by detect_delete
	$db = db_connect();
	$stmt = $db->prepare('INSERT INTO `invalid_node`(`node_id`, `lat`, `lon`) VALUES (?, ?, ?)');
	try {
		$stmt->execute([$node, $lat, $lon]);
	}
	catch(PDOException $ex) {
		//do nothing
	}
	$db = NULL;
}

function split_changeset(int $sum) {
	//an OSM changeset cannot contain more than 10.000 edits.
	//if we have more than 9.000 changes, randomly discard 50% of the changes
	//if we have more than 19.000 changes, randomly discard 67% of the changes
	//if we have more than 29.000 changes, randomly discard 75% of the changes
	//if we have more than 39.000 changes, randomly discard 80% of the changes
	//if CHANCE is not defined as 100 (meaning that not all edits for the given postcode are submitted in the current changeset),
	//the same postcode will be selected again next time the script is run.
	if($sum > 39000) {
		define('CHANCE', 20);
	}
	elseif($sum > 29000) {
		define('CHANCE', 25);
	}
	elseif($sum > 19000) {
		define('CHANCE', 33);
	}
	elseif($sum > 9000) {
		define('CHANCE', 50);
	}
	else {
		define('CHANCE', 100);
	}
}

function get_addr_tags(): array {
	//return an array of all recognised address tags, i.e. tags we are comfortable deleting in case we need to remove an address
	//NOTE!!! if updating this list, also update the $xpath_string variable in delete_nodes()
	return array(
		"addr:street",
		"addr:housenumber",
		"osak:house_no",
		"osak:street_name",
		"osak:identifier",
		"addr:city",
		"addr:country",
		"addr:housenumber",
		"addr:postcode",
		"osak:municipality_name",
		"addr:municipality",
		"osak:revision",
		"osak:municipality_no",
		"osak:street_no",
		"source",
		"osak:street",
		"osak:subdivision",
		"addr:place",
		"ois:fixme",
		"fixme"
	);
}

## Functions used to work with data from the database and submit it to the OSM API ##

function create_nodes() {
	//submits to the OSM API new addresses as imported from add_addr
	$db = db_connect();
	$stmt = $db->prepare('SELECT * FROM `add_addr`');
	$stmt->execute();
	$results = $stmt->fetchAll();
	$ch = curl_init();
	curl_setopt_array($ch, array(
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
		CURLOPT_USERPWD => USERNAME.":".PASSWORD,
		CURLOPT_CUSTOMREQUEST =>"PUT",
		CURLOPT_URL => URL.'node/create')
	);
	foreach($results as $result) { //OSM API only allows for creating one node at the time, so we have to call the API once for every single node
		$node = new SimpleXMLElement('<osm><node></node></osm>');
		$node->children()[0]->addAttribute('changeset', CHANGESET);
		$node->children()[0]->addAttribute('lat', $result['lat']);
		$node->children()[0]->addAttribute('lon', $result['lon']);
		$osak_id = id_format($result['osak:identifier']);
		$city = $result['addr:city'];
		$country = $result['addr:country'];
		$house_no = $result['addr:housenumber'];
		$postcode = $result['addr:postcode'];
		$street = $result['addr:street'];
		$municipality_name = $result['addr:municipality'];
		$place = $result['addr:place'];
		add_tag($node, "source=Danmarks Adresseregister");
		add_tag($node, "osak:identifier={$osak_id}");
		add_tag($node, "addr:city={$city}");
		add_tag($node, "addr:country={$country}");
		add_tag($node, "addr:housenumber={$house_no}");
		add_tag($node, "addr:postcode={$postcode}");
		add_tag($node, "addr:street={$street}");
		add_tag($node, "addr:municipality={$municipality_name}");
		if($place !== NULL) {
			add_tag($node, "addr:place={$place}");
		}
		$xml = html_entity_decode($node->asXML());
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
		if(!DEBUG AND rand(0,100) <= CHANCE) {
			curl_exec($ch);
		}
	}
	$db = NULL;
}

function detect_parent_ways($node_id): bool {
	//returns TRUE if a given node is part of a way, or FALSE if the given node is standalone (not part of a way)
	$parent_ways = new SimpleXMLElement(file_get_contents('https://api.openstreetmap.org/api/0.6/node/' . $node_id . '/ways'));
	if($parent_ways->children()[0] == NULL) {
		return FALSE;
	}
	return TRUE;
}

function update_node_in_way($result): bool {
	//if a node is a member of a way, we do not want to update the node (since moving the node might deform the way)
	//instead, remove all address tags from the node in question, and create a new standalone node with the updated address values
	
	//create a new node with the address
	$ch = curl_init();
	curl_setopt_array($ch, array(
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
		CURLOPT_USERPWD => USERNAME.":".PASSWORD,
		CURLOPT_CUSTOMREQUEST =>"PUT",
		CURLOPT_URL => URL.'node/create')
	);
	$node = new SimpleXMLElement('<osm><node></node></osm>');
	$node->children()[0]->addAttribute('changeset', CHANGESET);
	$node->children()[0]->addAttribute('lat', $result['lat']);
	$node->children()[0]->addAttribute('lon', $result['lon']);
	$osak_id = id_format($result['osak:identifier']);
	$city = $result['addr:city'];
	$country = $result['addr:country'];
	$house_no = $result['addr:housenumber'];
	$postcode = $result['addr:postcode'];
	$street = $result['addr:street'];
	$municipality_name = $result['addr:municipality'];
	$place = $result['addr:place'];
	add_tag($node, "source=Danmarks Adresseregister");
	add_tag($node, "osak:identifier={$osak_id}");
	add_tag($node, "addr:city={$city}");
	add_tag($node, "addr:country={$country}");
	add_tag($node, "addr:housenumber={$house_no}");
	add_tag($node, "addr:postcode={$postcode}");
	add_tag($node, "addr:street={$street}");
	add_tag($node, "addr:municipality={$municipality_name}");
	if($place !== NULL) {
		add_tag($node, "addr:place={$place}");
	}
	$xml = html_entity_decode($node->asXML());
	curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
	if(!DEBUG) {
		curl_exec($ch);
	}
	unset($ch, $node);
	$node = new SimpleXMLElement('<osm>'.$result['xml'].'</osm>');
	
	//remove all address tags from the existing node (which is part of a way)
	$ch = curl_init();
	curl_setopt_array($ch, array(
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
		CURLOPT_USERPWD => USERNAME.":".PASSWORD,
		CURLOPT_CUSTOMREQUEST => "PUT"
		));
	if(!$node) {
		return FALSE;
	}
	$node->node[0]['changeset'] = CHANGESET;
	$node_string = (string) $node->asXML();
	$valid_tags = get_addr_tags();
	foreach($valid_tags as $tag) {
		$node_string = preg_replace('/<tag k="' . $tag . '" v=".+"\/>/', '', $node_string);
	}
	curl_setopt($ch, CURLOPT_URL, URL.'node/'.$result['node_id']);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $node_string);
	if(!DEBUG) {
		curl_exec($ch);
	}
	return TRUE;
}

function delete_node_in_way($node, $id): bool {
	//remove all address tags from a node which is part of a way (delete the address without actually deleting the node)
	$ch = curl_init();
	curl_setopt_array($ch, array(
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
		CURLOPT_USERPWD => USERNAME.":".PASSWORD,
		CURLOPT_CUSTOMREQUEST => "PUT"
		));
	$node->node[0]['changeset'] = CHANGESET;
	$node_string = (string) $node->asXML();
	$valid_tags = get_addr_tags();
	foreach($valid_tags as $tag) {
		$node_string = preg_replace('/<tag k="' . $tag . '" v=".+"\/>/', '', $node_string);
	}
	curl_setopt($ch, CURLOPT_URL, URL.'node/'.$id);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $node_string);
	if(!DEBUG) {
		curl_exec($ch);
	}
	return TRUE;
}

function update_nodes() {
	$db = db_connect();
	$stmt = $db->prepare('SELECT * FROM `update_addr`');
	$stmt->execute();
	$results = $stmt->fetchAll();
	$db = NULL;
	$ch = curl_init();
	curl_setopt_array($ch, array(
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
		CURLOPT_USERPWD => USERNAME.":".PASSWORD,
		CURLOPT_CUSTOMREQUEST => "PUT"
		));
	$error = array();
	foreach($results as $result) {
		$node = new SimpleXMLElement('<osm>'.$result['xml'].'</osm>');
		if(!$node) {
			continue; //not able to get node, skip to the next one
		}
		if(detect_parent_ways($result['node_id'])) {
			update_node_in_way($result);
			continue;
		}
		$node->node[0]['lat'] = $result['lat'];
		$node->node[0]['lon'] = $result['lon'];
		$node->node[0]['changeset'] = CHANGESET;
		
		$node_string = (string) $node->asXML();
		//it's easier to do simple regex replace when updating tag values instead of having to search through <tag> XML elements
		$missing_tags = array(
		'addr:city'=>FALSE,
		'addr:housenumber'=>FALSE,
		'addr:street'=>FALSE,
		'addr:municipality'=>FALSE,
		'addr:postcode'=>FALSE,
		'addr:place'=>FALSE,
		'addr:country'=>FALSE
		);
		$node_string = preg_replace('/<tag k="osak:identifier" v=".+"\/>/', '<tag k="osak:identifier" v="'.id_format($result['osak:identifier']).'"/>', $node_string);
		$node_string = preg_replace('/<tag k="addr:city" v=".+"\/>/', '<tag k="addr:city" v="'.$result['addr:city'].'"/>', $node_string, -1, $count);
		if($count == 0) {
			$missing_tags['addr:city'] = TRUE;
		}
		$node_string = preg_replace('/<tag k="addr:housenumber" v=".+"\/>/', '<tag k="addr:housenumber" v="'.$result['addr:housenumber'].'"/>', $node_string, -1, $count);
		if($count == 0) {
			$missing_tags['addr:housenumber'] = TRUE;
		}
		$node_string = preg_replace('/<tag k="addr:street" v=".+"\/>/', '<tag k="addr:street" v="'.$result['addr:street'].'"/>', $node_string, -1, $count);
		if($count == 0) {
			$missing_tags['addr:street'] = TRUE;
		}
		$node_string = preg_replace('/<tag k="addr:municipality" v=".+"\/>/', '<tag k="addr:municipality" v="'.$result['addr:municipality'].'"/>', $node_string, -1, $count);
		if($count == 0) {
			$missing_tags['addr:municipality'] = TRUE;
		}
		$node_string = preg_replace('/<tag k="addr:postcode" v=".+"\/>/', '<tag k="addr:postcode" v="'.$result['addr:postcode'].'"/>', $node_string, -1, $count);
		if($count == 0) {
			$missing_tags['addr:postcode'] = TRUE;
		}
		if($result['addr:place'] !== NULL ){
			$node_string = preg_replace('/<tag k="addr:place" v=".+"\/>/', '<tag k="addr:place" v="'.$result['addr:place'].'"/>', $node_string, -1, $count);
			if($count == 0) {
				$missing_tags['addr:place'] = TRUE;
			}
		}
		if($result['addr:place'] == NULL ){
			$node_string = preg_replace('/<tag k="addr:place" v=".+"\/>/', '', $node_string);
		}
		$node_string = preg_replace('/<tag k="addr:country" v=".+"\/>/', '<tag k="addr:country" v="DK"/>', $node_string, -1, $count);
		if($count == 0) {
			$missing_tags['addr:country'] = TRUE;
		}
		$node_string = preg_replace('/<tag k="osak:subdivision" v=".+"\/>/', '', $node_string);
		$node_string = preg_replace('/<tag k="osak:house_no" v=".+"\/>/', '', $node_string);
		$node_string = preg_replace('/<tag k="osak:municipality_no" v=".+"\/>/', '', $node_string);
		$node_string = preg_replace('/<tag k="osak:municipality_name" v=".+"\/>/', '', $node_string);
		$node_string = preg_replace('/<tag k="osak:street_name" v=".+"\/>/', '', $node_string);
		$node_string = preg_replace('/<tag k="osak:street_no" v=".+"\/>/', '', $node_string);
		$node_string = preg_replace('/<tag k="osak:street" v=".+"\/>/', '', $node_string);
		$node_string = preg_replace('/<tag k="osak:revision" v=".+"\/>/', '', $node_string);
		
		//the following if is run if one of the seven tags defined in $missing_tags is not found on the given node
		//the node (currently represented as an XML string) is then converted to an XML-object, the missing tag is added, and it is converted back to an xml-string
		if(array_search(TRUE, $missing_tags) != FALSE) {
			$node = new SimpleXMLElement($node_string);
			foreach($missing_tags AS $tag => $missing) {
				if($missing) {
					add_tag($node, $tag . '=' . $result[$tag]);
				}
			}
			$node_string = (string) $node->asXML();
		}
		
		curl_setopt($ch, CURLOPT_URL, URL.'node/'.$result['node_id']);
		$xml = html_entity_decode($node_string);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $node_string);
		if(!DEBUG AND rand(0,100) <= CHANCE) {
			curl_exec($ch);
		}
	}
	if(!empty($error)) {
		output(9, 'Der var problemer med følgende adresser, de bør tjekkes manuelt:');
		foreach($error AS $err) {
			output(2, $err);
		}
	}
}

function update_node(string $xml_node, int $id) {
	$ch = curl_init();
	curl_setopt_array($ch, array(
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
		CURLOPT_USERPWD => USERNAME.":".PASSWORD,
		CURLOPT_CUSTOMREQUEST => "PUT",
		CURLOPT_URL => URL.'node/'.$id,
		CURLOPT_POSTFIELDS => $xml_node
		));
	if(!DEBUG) {
		curl_exec($ch);
	}
}

function delete_nodes() {
	//these are the tags we are happy with deleting. If any OTHER tags exist, we want to preserve them
	$xpath_string = 'node/tag[not(
		@k="addr:street"
		or @k="addr:housenumber"
		or @k="osak:house_no"
		or @k="osak:street_name"
		or @k="osak:identifier"
		or @k="addr:city"
		or @k="addr:country"
		or @k="addr:housenumber"
		or @k="addr:postcode"
		or @k="osak:municipality_name"
		or @k="addr:municipality"
		or @k="osak:revision"
		or @k="osak:municipality_no"
		or @k="osak:street_no"
		or @k="source"
		or @k="osak:street"
		or @k="osak:subdivision"
		or @k="addr:place"
		or @k="ois:fixme"
		or @k="fixme"
	)]';
	$valid_tags = get_addr_tags();
	$db = db_connect();
	$stmt = $db->prepare('SELECT * FROM `delete_addr`');
	$stmt->execute();
	$results = $stmt->fetchAll();
	foreach($results as $result) { //iterate over all nodes with addresses that are to be deleted
		$id = (int) $result['node_id'];
		$node = new SimpleXMLElement('<osm>'.$result['xml'].'</osm>');
		if(!$node) {
			continue; //not able to get node, skip to the next one
		}
		//in case an address has been moved from one postcode to another (position remains constant but postcode changes),
		//we will delete the address node, but then make sure the new postcode is updated ASAP so the address can be added again
		update_postcode_deleted_node($result['lat'], $result['lon']);
		$xpath = $node->xpath($xpath_string); //check if the node has tags that should be preserved
		if(!empty($xpath)) { //node has additional tags that should be preserved
			$tags = count($node->children()[0]->children());
			for($i = $tags; $i >= 0; --$i) {
				if(in_array(strval($node->children()[0]->tag[$i]['k']), $valid_tags)) { //remove all address nodes, leaving only the extra nodes
					$dom = dom_import_simplexml($node->children()[0]->tag[$i]);
					$dom->parentNode->removeChild($dom);
				}
			}
			//at this point, all address tags are deleted
			$node->children()[0]['changeset'] = CHANGESET;
			add_tag($node, 'fixme=This address no longer exists. Please check if the tags on this node are still valid');
			update_node(html_entity_decode($node->asXML()), $id);
		}
		else { //no additional tags, just delete the node
			$node->children()[0]['changeset'] = CHANGESET;
			delete_node($node->asXML(), $id);
		}
	}
	$db = NULL;
}

function delete_node(string $xml_node, int $id) {
	$ch = curl_init();
	curl_setopt_array($ch, array(
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
		CURLOPT_USERPWD => USERNAME.":".PASSWORD,
		CURLOPT_CUSTOMREQUEST => "DELETE",
		CURLOPT_URL => URL.'node/'.$id,
		CURLOPT_POSTFIELDS => $xml_node
		));
	if(!DEBUG AND rand(0,100) <= CHANCE) {
		curl_exec($ch);
		$resp = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		switch($resp) {
			case 412:
				$node = new SimpleXMLElement($xml_node);
				delete_node_in_way($node, $id);
			break;
		}
	}
}

function update_postcode_deleted_node(float $lat, float $lon) {
	//given a lat,lon pair, this function checks if an address exists in that position in DAR and, if so, if that address is in another postcode than the one being updated currently
	//if so, that other postcode will be updated next. This is to ensure that if an address node is deleted as a result of its postcode changing, it will be added again quickly with the new postcode
	$addr = json_decode(file_get_contents('http://dawa.aws.dk/adgangsadresser?cirkel='.$lon.','.$lat.',1&struktur=mini'));
	if(!empty($addr)) {
		$postcode = (int) $addr[0]->postnr;
		if($postcode != POSTNUMMER AND !DEBUG) {
			output(2, 'Opdateringen påvirker postnummer ' . $postcode . ' som vil blive opdateret snarest');
			$db = db_connect();
			$db->exec('UPDATE `last_update` SET `last_update`.`opdateret` = "2017-01-01 00:00:01" WHERE `last_update`.`postnummer` = ' . $postcode);
			$db = NULL;
		}
	}
}

## Misc. functions ##

function opdater_postnumre () {
	$db = db_connect();
	$stmt = $db->prepare('SELECT `postnummer` FROM `last_update`');
	$stmt->execute();
	$db_postnumre = $stmt->fetchAll(PDO::FETCH_COLUMN);
	$aws_data = json_decode(file_get_contents('https://dawa.aws.dk/postnumre/'));
	$aws_postnumre = array();
	foreach($aws_data as $nummer) {
		array_push($aws_postnumre, intval($nummer->nr));
	}
	if(count($aws_postnumre) < 700) { //we expect around 1000 postcodes at least
		output(9, 'Kritisk vejl ved indlæsning af postnumre');
		die();
	}
	$delete = array_diff($db_postnumre, $aws_postnumre);
	$add = array_diff($aws_postnumre, $db_postnumre);
	//delete non-existing postcodes
	foreach($delete as $del) {
		$db->exec('UPDATE `last_update` SET `last_update`.`deleted` = 1 WHERE `last_update`.`postnummer` = '. $del); //when a postcode is discontinued, the deleted value is set to 1. Postcodes with a deleted value of 1 will be picked first for the next update. After being updated once, postcodes with a deleted value of 1 will be removed from the database.
	}
	//add missing postcodes
	$stmt = $db->prepare('INSERT INTO `last_update`(`postnummer`, `opdateret`) VALUES (?,?)');
	foreach($add as $a) {
		$stmt->execute([$a, '2017-01-01 00:00:00']); //sets a date in the past to force the script to update the adresses in this postcode as soon as possible
	}
	$db = NULL;
}

function select_postcode() {
	//select and define the next postcode to update
	if(MANUAL) {
		define('POSTNUMMER', MANUAL_CODE); return;
	}
	$db = db_connect();
	$stmt = $db->prepare('SELECT `postnummer`, `deleted` FROM `last_update` ORDER BY `deleted` DESC, `opdateret` ASC, `postnummer` ASC LIMIT 1');
	try {
		$stmt->execute();
		$fetch = $stmt->fetch();
		$postnr = (int) $fetch['postnummer'];
		if($postnr >= 1000 AND $postnr <= 9999) {
			define('POSTNUMMER', $postnr);
		}
		else {
			throw new Exception();
		}
		if($fetch['deleted'] == 1) {
			$db->exec('DELETE FROM `last_update` WHERE `last_update`.`postnummer` = '.POSTNUMMER);
		}
	}
	catch(Exception $ex) {
		opdater_postnumre();
		output(9, 'Kunne ikke indlæse postnumre');
		die();
	}
	if(POSTNUMMER % 1000 == 0) { //we need to make sure the list of postcodes is up to date
		output(2, 'Opdaterer listen af postnumre');
		opdater_postnumre();
	}
	$db = NULL;
}

function database_cleanup() {
	$db = db_connect();
	$db->exec('TRUNCATE TABLE `osmdata`;');
	$db->exec('TRUNCATE TABLE `awsdata`;');
	$db->exec('TRUNCATE TABLE `add_addr`;');
	$db->exec('TRUNCATE TABLE `delete_addr`;');
	$db->exec('TRUNCATE TABLE `update_addr`;');
	$db->exec('TRUNCATE TABLE `invalid_node`;');
	$db->exec('TRUNCATE TABLE `ois_fixes`;');
	$db->exec('TRUNCATE TABLE `xmlnodes`;');
	if(defined('CHANCE') AND CHANCE == 100 AND !DEBUG) {
		$db->exec('UPDATE `last_update` SET `last_update`.`opdateret` = "' . date("Y-m-d H:i:s") . '" WHERE `last_update`.`postnummer` = ' . POSTNUMMER);
	}
	$db = NULL;
}
